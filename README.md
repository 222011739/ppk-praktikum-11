> Nama : Muhammad Naufal Faishal            
Kelas / Absen : 3SI3 / 27

## Laporan Praktikum 11

1. Kegiatan Praktikum
        >![Call](/images/Call.png "Call")
        >![Setting](/images/Setting.png "Setting")

1. Penugasan
        >![Search](/images/Search.png "Search")
        Setelah menekan tombol Button 3, maka kita akan dialihkan ke google dan langsung dalam kondisi pencarian dengan keyword yang sudah di atur pada back-end.
        >![Audio](/images/Audio.png "Audio")
        Setelah menekan tombol Button 4, maka kita akan dialihkan ke aplikasi pengelola file untuk mencari dokumen yang dibutuhkan
        >![Form](/images/Form.png "Form")
        Setelah menekan tombol Button 5, maka kita akan dialihkan ke halaman pengisian form. Jika memencet tombol save akan dialihkan ke halaman lain.
        >![Bio](/images/Bio.png "Bio")
        Setelah menekan tombol save pada halaman sebelumnya, kita akan ditampilkan hasil dari apa yang telah diisikan pada halaman sebelumnya. Ketika kita menekan tombol back, maka kita akan dikembalikan ke halaman utama yang ada beberapa tombol tadi.
