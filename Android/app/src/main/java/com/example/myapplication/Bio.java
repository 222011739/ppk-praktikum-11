package com.example.myapplication;

import android.os.Parcel;
import android.os.Parcelable;

public class Bio implements Parcelable{
    String nama, hobi, umur;

    public Bio(){
    }

    public Bio(String nama, String hobi, String umur){
        this.nama = nama;
        this.hobi = hobi;
        this.umur = umur;
    }

    protected Bio(Parcel in) {
        nama = in.readString();
        hobi = in.readString();
        umur = in.readString();
    }

    public static final Creator<Bio> CREATOR = new Creator<Bio>() {
        @Override
        public Bio createFromParcel(Parcel in) {
            return new Bio(in);
        }

        @Override
        public Bio[] newArray(int size) {
            return new Bio[size];
        }
    };

    public String getNama() {
        return nama;
    }

    public String getHobi() {
        return hobi;
    }

    public String getUmur() {
        return umur;
    }

    public void setHobi(String hobi) {
        this.hobi = hobi;
    }

    public void setName(String nama) {
        this.nama = nama;
    }

    public void setUmur(String umur) {
        this.umur = umur;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(nama);
        parcel.writeString(hobi);
        parcel.writeString(umur);
    }
}
