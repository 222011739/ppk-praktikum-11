package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Form extends AppCompatActivity {
    private Button submitButton;
    TextView namaTv, hobiTv, umurTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);

        submitButton = (Button) findViewById(R.id.submitButton);
        namaTv = findViewById(R.id.namaInput);
        hobiTv = findViewById(R.id.hobiInput);
        umurTv = findViewById(R.id.umurInput);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nama = namaTv.getText().toString();
                String hobi = hobiTv.getText().toString();
                String umur = umurTv.getText().toString();
                Bio bio = new Bio(nama, hobi, umur);

                Intent intent = new Intent(Form.this, BioDetail.class);
                intent.putExtra("bio", bio);
                startActivity(intent);

            }
        });
    }


}