package com.example.myapplication;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class BioDetail extends AppCompatActivity {
    TextView namaTv, hobiTv, umurTv;
    Button backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bio_detail);

        namaTv = findViewById(R.id.namaOutput);
        hobiTv = findViewById(R.id.hobiOutput);
        umurTv = findViewById(R.id.umurOutput);
        backButton = findViewById(R.id.backButton);

        Bio bio = getIntent().getParcelableExtra("bio");
        namaTv.setText(bio.getNama());
        hobiTv.setText(bio.getHobi());
        umurTv.setText(bio.getUmur());

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(BioDetail.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }
}
