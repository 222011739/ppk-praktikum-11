package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.app.SearchManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button btn1,btn2,btn3,btn4,btn5;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn1 = (Button) findViewById(R.id.button1);
        btn2 = (Button) findViewById(R.id.button2);
        btn3 = (Button) findViewById(R.id.button3);
        btn4 = (Button) findViewById(R.id.button4);
        btn5 = (Button) findViewById(R.id.button5);
    }

    public void btnClick(View view) {
        Intent tlp = new Intent (Intent.ACTION_DIAL,
                Uri.parse("tel:081917558751"));
        startActivity(tlp);
    }

    public void btn2Click(View view) {
        Intent setting = new Intent(
                android.provider.Settings.ACTION_SETTINGS);
        startActivity(setting);
        Toast.makeText(this, "you have Pressed : " +
                btn2.getText(), Toast.LENGTH_LONG).show();
    }

    public void btn3Click(View view) {
        Intent search = new Intent (Intent.ACTION_WEB_SEARCH );
        search.putExtra(SearchManager.QUERY, "intent android");
        startActivity(search);
    }

    public void btn4Click(View view) {
        Intent myIntent= new Intent();
        myIntent.setType("audio/mp3");
        myIntent.setAction(Intent.ACTION_GET_CONTENT);
        startActivity(myIntent);
    }

    public void btn5Click(View view){
        Intent intent = new Intent(MainActivity.this, Form.class);
        startActivity(intent);
    }
}